import logging as log
import Adafruit_DHT
import multiprocessing as mp
import threading
import time
import datetime

class Sensor:
    """
    Sensor represents a wrapper around the dht package.
    Connection is hardware GPIO pins
    Polled Data is in custom String format
    If data cannot be polled, a log entry is made and that poll is skipped
    parameter delta is the time to wait in between polling for new data
    parameter stream is the multiprocessing.queues.Queue that this data is sent over

    """
    def __init__(self, delta, stream, chamber):
        assert isinstance(delta, (int, float)),"param delta is not a numeric type"
        assert isinstance(stream, (mp.queues.Queue)), "param stream is not a multiprocessing.queues.Queue"
        self.stream = stream
        self.delta = delta
        self._quit = False
        self._hasQuit = False
        self._timeout = 3 #time in seconds to wait for sensor to ready itself before no longer waiting
        self.chamber = chamber

        log.info('\n\nInitial reading:')
        d = self.pollSensor()
        if d is not None:
            log.info('{}'.format(d))
        else:
            log.info('{}'.format("error"))

    # Accepts as an argument a celsius value (float) and returns its fahrenheit equivalent (float)
    def ctof(self, t):
        return (t * float(9/5)) + 32

    # Polls the hardware sensor and returns the temperature and humitity values
    def pollSensor(self):
        h, t = Adafruit_DHT.read_retry(11, 4)
        if h is None or t is None:
            return None
        else:
            return str(self.ctof(float(t))) + ", " + str(h)

    """
    Start creates a new thread that starts the polling of the sensor and automated uploading to the stream. 
    Returns the Thread handle object.
    """
    def start(self):
        thread = threading.Thread(target=self._startBlocking)
        thread.start()
        return thread

    def _startBlocking(self):
        self._hasQuit = False
        log.info('\n\nStart Polling:')
        #continously poll until it's time to quit
        while not self._quit:
            data = self.pollSensor()
            now = datetime.datetime.now()
            if data is not None:
                data = self.chamber + " -- BLANK -- " + str(now.day) + "." + str(now.month) + "." + str(now.year) + " -- " + str(now.hour) + ":" + str(now.minute) + ":" + str(now.second) + " -- " + data
                #put data in queue
                try:
                    self.stream.put(data,timeout=self._timeout)
                except Exception as e:
                    if isinstance(e, mp.queues.Full):
                        log.warning("Queue Full, could not put data in queue. Data is being dropped.")
                    else:
                        log.error("Unexpected Exception encountered when placing sensor data in queue: {}".format(str(e)))
                #log data to debug
                log.debug(data)
            else:
                log.error(str(now.day) + "." + str(now.month) + "." + str(now.year) + " -- " + str(now.hour) + ":" + str(now.minute) + ":" + str(now.second) + " -- Sensor returned 'None'")
            #sleep until next polling time    
            sleepDatum = time.time()
            while time.time()-sleepDatum < self.delta:
                if self._quit:
                    break
                else:
                    time.sleep(0.1) #sleep 100 milliseconds        
        #set "hasQuit" flag
        self._hasQuit = True
    """
    Stop blocks until the sensor is no longer uploading data or it times out. 
    If it times out then an error is logged and false is returned otherwise True is returned.
    """
    def stop(self, timeout=5):
        self._quit = True
        datum = time.time()
        while time.time()-datum <= timeout:
            if self._hasQuit:
                return True
            time.sleep(0.1) #sleep 100 milliseconds
        else:
            log.error("Stopping the sensor timed out after {} seconds; The Sensor never stopped polling data".format(timeout))
            return False