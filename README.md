# Sensor Hardware

Collection of Tools that wrap python library's & APIs of sensors I use.

## Sensors

* [BME680](https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME680-DS001.pdf)
    * An Atmospheric Sensor made by Bosch that measures Temperature, Pressure, Relative Humidity & Air Quality. Price is [$22.50 per sensor](https://www.adafruit.com/product/3660).
