import bme680
import logging as log
import multiprocessing as mp
import threading
import time

#Some source code lifted from https://github.com/pimoroni/bme680-python/blob/master/examples/read-all.py

class Sensor:
    """
    Sensor represents a wrapper around the bme680 package.
    Connection is via I2C
    Polled data is in YAML format
    parameter delta is the time to wait in between polling for new data
    parameter stream is the multiprocessing.queues.Queue that this data is sent over
    If data cannot be polled then that polling session is skipped.
    If data cannot be placed into queue then it is dropped.
    """
    def __init__(self, delta, stream):
        assert isinstance(delta, (int, float)),"param delta is not a numeric type"
        assert isinstance(stream, (mp.queues.Queue)), "param stream is not a multiprocessing.queues.Queue"
        self.stream = stream
        self.delta = delta
        self._quit = False
        self._hasQuit = False
        self._gasCycle_milli = 50 #in Milliseconds
        self._gasCycle_sec = 0.001 * self._gasCycle_milli #in seconds
        self._timeout = 5 #time in seconds to wait for sensor to ready itself before no longer waiting

        #Setup Sensor
        try:
            self.device = bme680.BME680(bme680.I2C_ADDR_PRIMARY)
        except IOError:
            self.device = bme680.BME680(bme680.I2C_ADDR_SECONDARY)

        # These calibration data can safely be commented out, if desired.
        log.info('Calibration data:')
        for name in dir(self.device.calibration_data):
            if not name.startswith('_'):
                value = getattr(self.device.calibration_data, name)
                if isinstance(value, int):
                    log.info('{}: {}'.format(name, value))

        # These oversampling settings can be tweaked to change the balance between accuracy and noise in the data.
        self.device.set_humidity_oversample(bme680.OS_2X)
        self.device.set_pressure_oversample(bme680.OS_4X)
        self.device.set_temperature_oversample(bme680.OS_8X)
        self.device.set_filter(bme680.FILTER_SIZE_3)
        self.device.set_gas_status(bme680.ENABLE_GAS_MEAS)

        log.info('\n\nInitial reading:')
        for name in dir(self.device.data):
            value = getattr(self.device.data, name)

            if not name.startswith('_'):
                log.info('{}: {}'.format(name, value))

        self.device.set_gas_heater_profile(300, self._gasCycle_milli)

    """
    Start creates a new thread that starts the polling of the sensor and automated uploading to the stream. 
    Returns the Thread handle object.
    """
    def start(self):
        thread = threading.Thread(target=self._startBlocking)
        thread.start()
        return thread

    def _startBlocking(self):
        self._hasQuit = False
        log.info('\n\nStart Polling:')
        #continously poll until it's time to quit
        while not self._quit:
            #don't pull data till ready
            datum = time.time()
            while not self.device.get_sensor_data():
                time.sleep(0.1 * self._gasCycle_sec) #if not ready wait 1/10 gas cycle
                if time.time() - datum >= self._timeout or self._quit: #timeout condition
                    if not self._quit:
                        log.error("Sensor took too long to respond; Skipping this reading")
                    else:
                        log.info("Polling exiting early; time to quite")
                    break
            else: #sensor is ready 
                timestamp = time.ctime()
                #format data as yaml
                output = ("time: {4}\n"
                          "temp: {0:.2f} C\n"
                          "pres: {1:.2f} hPa\n"
                          "hum: {2:.3f} %\n"
                          "gas: {3:.3f} Ω\n").format(self.device.data.temperature, self.device.data.pressure, self.device.data.humidity, self.device.data.gas_resistance, timestamp)
                
                #put data in queue
                try:
                    self.stream.put(output,timeout=self._timeout)
                except Exception as e:
                    if isinstance(e, mp.queues.Full):
                        log.warning("Queue Full, could not put data in queue. Data is being dropped.")
                    else:
                        log.error("Unexpected Exception encountered when placing sensor data in queue: {}".format(str(e)))
                
                #log data to debug
                log.debug(output)
            
            #sleep until next polling time    
            sleepDatum = time.time()
            while time.time()-sleepDatum < self.delta:
                if self._quit:
                    break
                else:
                    time.sleep(0.1) #sleep 100 milliseconds
        
        #set "hasQuit" flag
        self._hasQuit = True

    """
    Stop blocks until the sensor is no longer uploading data or it times out. 
    If it times out then an error is logged and false is returned otherwise True is returned.
    """
    def stop(self, timeout=5):
        self._quit = True
        datum = time.time()
        while time.time()-datum <= timeout:
            if self._hasQuit:
                return True
            time.sleep(0.1) #sleep 100 milliseconds
        else:
            log.error("Stopping the sensor timed out after {} seconds; The Sensor never stopped polling data".format(timeout))
            return False

